# McFly & Carlito BOT

Bot pour le serveur Discord de McFly & Carlito

## Installation

1. Ouvrir le fichier `config.json` et configurer le fichier en fonction du serveur avec l'ID des bons channels. Il faut modifier les informations entre les guillemets.

2. Ouvrir la console (ou le CMD) et lancer la commande :

```bash
docker-compose build && docker-compose up
```

## Config.json

Le fichier `config.json` contient tous les paramètres du bot. Voici sa structure :

```json
{
    "token": "TOKEN DU BOT DISCORD",
    "prefixCommand": "PREFIX DES COMMANDES (ex: /)",
    "channels": {
        "__comment__":  "Cette section contient l'ID des channels utilisés par le bot.",
        "sas_animation": "772953285786861572",
        "live": "772963733852782624",
        "sas_attente": "772962423853809775"
    },
    "roles": {
        "__comment__":  "Cette section contient l'ID des rôles utilisés par le bot",
        "participant": "772959294831591504",
        "viewer": "772964977866309632"
    },
    "groups": {
        "__comment__":  "Cette section contient des groupes de roles. Un groupe permet d'établir un groupe de permission pour les commandes. Ajouter autant de groupes que nécessaire.",
        "animateurs": [
            "772959294831591504",
            "772959294831591504"
        ]
    },
    "colors": {
        "__comment__": "Défini les couleurs que le bot doit utiliser pour afficher les messages d'informations en fonction des commandes.",
        "animateurs": "#69D493"
    }
}
````

## Permissions

Voici la liste des permissions nécessaires au bot :

- Manage Roles
- Manage Channels
- Manage Messages