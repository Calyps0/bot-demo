FROM node:latest

WORKDIR /usr/app/bot

COPY . ./
RUN npm install

EXPOSE 3571

CMD ["npm", "run", "dev"]