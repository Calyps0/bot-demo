const Discord = require('discord.js');

const Sanctions = require('../../models/sanctions');
const userUtils = require("../../commons/user");

const { groups } = require.main.require('./config.json');

function roundDecimal(nombre, precision){
    precision = precision || 2;

    var tmp = Math.pow(10, precision);
    
    return Math.round( nombre*tmp )/tmp;
}

module.exports = {
    name: "prono",
    description: "Permet de donner un pronostic",
    args: true,
    usages: [
        { name: "Miser des jetons sur une équipe", usage: "<Numéro du match> <Nom de l'équipe> <Mise>" },
        { name: "Obtenir son prono pour le match X", usage: "get <Numéro du match>" },
        { name: "Supprimer un prono", usage: "delete <Numéro du match>" }
    ],
    examples: [
        { name: "Miser 5 jetons sur la France pour le match n°3 (France - Allemagne)", usage: "3 France 5" },
        { name: "Obtenir son prono pour le match n°3 (France - Allemagne)", usage: "get 3" },
        { name: "Supprimer son prono pour le match n°3", usage: "delete 3"}
    ],
    async execute(client, message, args) {


        /*
         * CHECK DES ARGUMENTS
         */
        if (args.length !== 3) {
            return await message.reply("le format de la commande est incorrect. Effectuer la commande /prono pour l'aide.");
        }

        var [id, prono, money] = args;
        const match = await client.pronos.getMatch(id);
        
        prono = prono.toLowerCase();

        if (!match) {
            return await message.reply(`le match n°${id} n'existe pas.`);
        }

        if (!match.open) {
            return await message.reply(`les pronos sont fermés pour ce match.`);
        }

        if (isNaN(money)) {
            return await message.reply(`votre mise doit être une valeur numérique.`);
        }

        if (money <= 0) {
            return await message.reply("votre mise doit être supérieure à 0.");
        }

        if (prono !== match.team1.toLowerCase() && prono !== match.team2.toLowerCase()) {
            return await message.reply(`l'équipe \`${prono}\` n'est pas reconnue. Vérifiez le match dans <#853033807372222474>.`);
        }

        if (await client.pronos.haveProno(id, message.author.id)) {
            return await message.reply(`vous avez déjà posé un prono pour ce match. Utilisez \`/prono delete <Numéro du match>\` pour supprimer votre ancien prono`)
        }

        const walletAmount = await client.pronos.getWallet(message.author.id);

        if (money > walletAmount) {
            return await message.reply(`vous n'avez pas assez de jetons pour miser cette somme.`);
        }

        /*
         * Exécution du pronostic
         */

        await client.pronos.substractWallet(message.author.id, money);
        await client.pronos.pushProno(id, message.author.id, prono, money);

        var potentialGain = 0;

        if (prono === match.team1.toLowerCase()) potentialGain = money * match.rate1;
        else if (prono === match.team2.toLowerCase()) potentialGain = money * match.rate2;
        else potentialGain = money * match.nullRate;

        prono = prono.charAt(0).toUpperCase() + prono.slice(1);

        await message.channel.send(new Discord.MessageEmbed()
            .setColor("#2598a1")
            .setTitle(`Match n°${match.matchId} • ${match.team1} - ${match.team2}`)
            .setThumbnail('https://upload.wikimedia.org/wikipedia/fr/3/32/UEFA_Euro_2020_logo.png')
            .addFields(
                { name: `Date du match`, value: match.date },
                { name: `Parieur`, value: message.author, inline: true },
                { name: `Mise`, value: `${money} :coin: sur **${prono}**`, inline: true },
                { name: `Gain potentiel`, value: roundDecimal(potentialGain), inline: true },
            )
        );
    },
};