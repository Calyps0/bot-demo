const Discord = require('discord.js');

const userUtils = require("../../commons/user");

module.exports = {
    name: "top",
    description: "Permet d'afficher le top 10 des parieurs pour l'Euro 2020",
    args: false,
    usages: [
        { name: "Obtenir le top 10 des parieurs", usage: "" }
    ],
    examples: [
        { name: "Obtenir le top 10 des parieurs", usage: "" },
    ],
    async execute(client, message, args) {
        const top = await client.pronos.getTop10();
        var content = "";

        /*
         * Création du visuel du top
         */
        for (const [id, user] of top.entries()) {
            const member = await userUtils.getMemberFromId(client, message, user.userId);
            const amount = Math.round(user.amount * 100) / 100;
            const memberScore = `**${member.displayName}** : ${amount}`;

            switch (id) {
                case 0:
                    content += `\n**:first_place: - ** ${memberScore}`;
                    break;
                case 1:
                    content += `\n\n**:second_place: - ** ${memberScore}`;
                    break;
                case 2:
                    content += `\n\n**:third_place: - ** ${memberScore}`;
                    break;
                default:
                    content += `\n\n** ${id + 1} - ** ${memberScore}`;
                    break;
            }
        }

        return await message.channel.send(new Discord.MessageEmbed()
            .setColor("#2598a1")
            .setTitle(`🏆 Classement des parieurs fous 🏆`)
            .setThumbnail('https://upload.wikimedia.org/wikipedia/fr/3/32/UEFA_Euro_2020_logo.png')
            .setDescription("―――――――――――――――――――― \n" + content)
        );
    }
};
