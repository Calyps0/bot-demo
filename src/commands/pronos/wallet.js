module.exports = {
    name: "wallet",
    description: "Permet de voir ses jetons",
    args: false,
    usages: [
        { name: "Supprimer la sanction d'un utilisateur.", usage: "<Mention de l'user> <ID de l'user dans Modlogs>" }
    ],
    examples: [
        { name: "Supprimer la sanction 3 de Thimothé", usage: "@Thimothé 3" }
    ],
    async execute(client, message, args) {
        var amount = await client.pronos.getWallet(message.author.id);

        amount = Math.round(amount * 100) / 100;
        return await message.reply(`vous avez ${amount} :coin:`);
    },
};
