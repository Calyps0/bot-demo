const { channels } = require.main.require('./config.json');

const remind = [];

module.exports = {
    name: "remind",
    description: "Permet d'être rappeler par MP de voter",
    args: false,
    async execute(client, message, args) {

        if (message.channel.id !== channels.vote) return;

        if (remind.includes(message.author.id)) {
            return await message.reply('vous avez déjà un rappel en cours');
        } else {

            remind.push(message.author.id);

            setTimeout(async () => {
                await message.author.send('Psssttttt, c\'est l\'heure de voter pour le serveur de McFly & Carlito !\nRendez-vous dans <#837719223572627476>');
                remind.splice(remind.indexOf(message.author.id), 1);
            }, 60 * 60 * 1000);

            return await message.reply('je te rappellerai de voter dans **1 heure** !');
        }
    },
};
