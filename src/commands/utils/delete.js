const { groups, channels } = require.main.require('./config.json');

module.exports = {
	name: "delete",
    description: "Permet de supprimer les X derniers messages d'un salon",
    args: true,
    usages: [
        { name: "Supprimer X messages", usage: "<Nombre>" }
    ],
    examples: [
        { name: "Supprimer les 10 derniers messages", usage: "10" },
    ],
    roles: groups.clear_command,
	async execute(client, message, args) {

        if (
            message.channel.id === channels.staff ||
            message.channel.id === channels.modos ||
            message.channel.id === channels.reunion ||
            message.channel.id === channels.modlogs ||
            message.channel.id === channels.sanctions ||
            message.channel.id === channels.modlogs
        ) {
            return;
        }

        if (args.length > 1) {
            return await message.channel.send(":1234: Le nombre de messages à supprimer n'est pas précisé.");
        }

        var amount = parseInt(args[0]);

        if (isNaN(amount)) {
            return await message.channel.send(":1234: L'argument de la commande est invalide.");
        } else if (amount < 0) {
            return await message.channel.send(":1234: Le nombre de messages doit être positif.");
        }

        amount++;

        var count = 0;

        do {
            var nb = amount >= 100 ? 100 : amount;
            let messages = await message.channel.bulkDelete(nb);

            if (messages.size === 0) break;

            count += messages.size;
            amount -= 100;
            nb = amount >= 100 ? 100 : amount;
        } while (100 < amount);

        const confirmMessage = await message.channel.send(`:broom: ${count} message(s) supprimé(s)`);

        setTimeout(() => {
            confirmMessage.delete();
        }, 3000);
	},
};