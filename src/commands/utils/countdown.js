const { groups, channels } = require.main.require('./config.json');

const ms = require('ms');
const { delay } = require.main.require('./commons/utils.js');

const numbers = {
    0: ":zero:",
    1: ":one:",
    2: ":two:",
    3: ":three:",
    4: ":four:",
    5: ":five:",
    6: ":six:",
    7: ":seven:",
    8: ":eight:",
    9: ":nine:"
};

const getNumbersEmoji = (number) => {
    let time = number.toString();
    let result = "";

    for (let digit of time) {
        result += numbers[digit];
    }
    return result;
};

var isUsing = false;

module.exports = {
	name: "countdown",
    description: "Permet de lancer un compte à rebours qui se désincrémente en direct. L'unité tu timer peut-être précisée (minute par défaut)",
    args: true,
    usages: [
        { name: "Créer un timer", usage: "<Temps [s|m|h])>" },
        { name: "Stopper le timer", usage: "stop" },
    ],
    examples: [
        { name: "Lancer un timer de 6 minutes", usage: "6" },
        { name: "Lancer un timer de 30 secondes", usage: "30s" },
        { name: "Lancer un timer d'une heure", usage: "1h" },
        { name: "Stopper un timer en cours", usage: "stop" },
    ],
    roles: groups.animateurs,
	async execute(client, message, args) {

        if (message.channel.id != channels.jeux) return;

        // Gestion du Timer
        if (isUsing) {
            if (args[0] === "stop") {
                isUsing = false;
                return await message.channel.send(":timer: Le timer a bien été arrêté.");
            }
            return await message.channel.send(":timer: Un timer est déjà en cours. Utiliser /countdown stop pour stopper le timer en cours.");
        } else if (args[0] === "stop") {
            return await message.channel.send(":timer: Aucun timer n'est en cours.");
        }

        // Gesion de l'argument Time
        var duration = isNaN(args[0]) ? ms(args[0]) : ms(args[0] + 'm');

        if (!duration) {
            return await message.channel.send(":timer: La durée du compte à rebours n'est pas valide.");
        } else if (duration <= 0) {
            return await message.channel.send(":timer: La durée du compte à rebours doit être supérieur à 0.");
        }

        var timer;

        duration /= 1000;
        isUsing = true;

        while (isUsing && duration > 0) {

            minutes = parseInt(duration / 60, 10);
            seconds = parseInt(duration % 60, 10);
            minutes = getNumbersEmoji(minutes < 10 ? "0" + minutes : minutes);
            seconds = getNumbersEmoji(seconds < 10 ? "0" + seconds : seconds);

            if (!timer) {
                await message.delete();
                timer = await message.channel.send(minutes + ' : ' + seconds);
            } else {
                try {
                    await timer.edit(minutes + " : " + seconds);
                } catch(e) {
                    isUsing = false;
                    return;
                }
            }

            duration -= 2;

            await delay(2000);

            if (duration <= 0) {
                try {
                    await timer.edit(":zero::zero: : :zero::zero:");
                } finally {
                    isUsing = false;
                    return;
                }
            }
        }
	},
};