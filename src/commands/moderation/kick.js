const dedent = require('dedent-js');

const userUtils = require("../../commons/user");

const { groups } = require.main.require('./config.json');

module.exports = {
	name: "kick",
    description: "Kick un utilisateur",
    args: true,
    usages: [
        { name: "Kick un utilisateur", usage: "<Mention de l'user> <Raison>" }
    ],
    examples: [
        { name: "Kick Thimothé", usage: "@Thimothé Troll" }
    ],
    roles: groups.moderateurs,
	async execute(client, message, args) {

        /*
         * Gestion de la commande KICK
        */
        if (args.length === 1) {
            return message.channel.send(":clipboard: La raison du kick est obligatoire.");
        }

        const member = await userUtils.getUserFromMention(message, args[0]);

        if (!member) {
            return message.channel.send(":x: L'utilisateur n'a pas été trouvé.");
        } else if (!member.kickable) {
            return message.channel.send(":x: Cet utilisateur ne peut pas être kick");
        } else if (member.user.bot) {
            return;
        }

        const reason = args.slice(1).join(' ');

        /*
         * Sauvegarde en base de données
        */
        await client.moderation.addSanction('KICK', message.author.tag, member, reason);

        /*
         * Gestion de la notification
        */
        const modNotif = client.moderation.getSanctionEmbed('#ff470f', 'KICK', message.author, member.user, reason);
        const userNotif = dedent`Vous avez été **kick** du serveur McFly & Carlito pour le motif suivant: *${reason}*
        Merci de relire le règlement du serveur.`;

        await message.delete();
        await userUtils.sendNotificationToUser(member.user, userNotif, modNotif);
        await message.channel.send(modNotif);
        await member.kick(reason);
	},
};
