const dedent = require('dedent-js');

const userUtils = require("../../commons/user");

const { groups } = require.main.require('./config.json');

module.exports = {
	name: "warn",
    description: "Warn un utilisateur",
    args: true,
    usages: [
        { name: "Warn un user", usage: "<Mention de l'user> <Raison>" }
    ],
    examples: [
        { name: "Warn Thimothé", usage: "@Thimothé Publicité" }
    ],
    roles: groups.moderateurs,
	async execute(client, message, args) {

        /*
         * Gestion de la commande WARN
        */
        if (args.length === 1) {
            return await message.channel.send(":clipboard: La raison du warn est obligatoire.");
        }
        
        const member = await userUtils.getUserFromMention(message, args[0]);

        if (!member) {
            return await message.channel.send(":x: L'utilisateur n'a pas été trouvé.");
        } else if (userUtils.checkRoleInList(member, groups.moderateurs)) {
            return await message.channel.send(":x: Tu ne peux pas warn un modérateur.");
        } else if (member.user.bot) {
            return;
        }

        const reason = args.slice(1).join(' ');

        /*
         * Sauvegarde en base de données
        */
        await client.moderation.addSanction('WARN', message.author.tag, member, reason);

        /*
         * Gestion des notifications Moderateur & Membre
        */
        const modNotif = client.moderation.getSanctionEmbed('#faa51b', 'WARN', message.author, member.user, reason);
        const userNotif = dedent`Vous avez reçu un **warn** sur le serveur McFly & Carlito pour le motif suivant: *${reason}*
        Merci de relire le règlement du serveur.`;

        await message.delete();
        await userUtils.sendNotificationToUser(member.user, userNotif, modNotif);
        await message.channel.send(modNotif);
	},
};
