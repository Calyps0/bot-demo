const dedent = require('dedent-js');

const userUtils = require("../../commons/user");

const { groups } = require.main.require('./config.json');

module.exports = {
	name: "ban",
    description: "Ban un user",
    args: true,
    usages: [
        { name: "Ban un user", usage: "<Mention de l'user> <Raison>" }
    ],
    examples: [
        { name: "Ban Thimothé", usage: "@Thimothé Troll" }
    ],
    roles: groups.moderateurs,
	async execute(client, message, args) {

        /*
         * Gestion de la commande BAN
        */
        if (args.length === 1) {
            return await message.channel.send(":clipboard: La raison du ban est obligatoire.");
        }

        const member = await userUtils.getUserFromMention(message, args[0]);

        if (!member) {
            return await message.channel.send(":x: L'utilisateur n'a pas été trouvé.");
        } else if (!member.bannable) {
            return await message.channel.send(":x: Cet utilisateur ne peut pas être ban");
        } else if (member.user.bot) {
            return;
        }

        const reason = args.slice(1).join(' ');

        /*
         * Gestion des notifications Moderateur & Membre
        */
        const modNotif = client.moderation.getSanctionEmbed('#ff470f', 'BAN', message.author, member.user, reason);
        const userNotif = dedent`Vous avez été **ban** du serveur McFly & Carlito pour le motif suivant: *${reason}*`;

        await message.delete();
        await userUtils.sendNotificationToUser(member.user, userNotif, modNotif);
        await message.channel.send(modNotif);
        await member.ban({ reason });
	},
};
