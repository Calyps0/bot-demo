const Discord = require('discord.js');
const moment = require('moment-timezone');

const { roles } = require.main.require('./config.json');
const userUtils = require.main.require("./commons/user");

const Sanctions = require('../../models/sanctions');

module.exports = class Moderation {

    async addSanction(type, moderator, member, reason, duration) {
        const muted = type === 'MUTE' ? true : false;
        var sanction = {
            date: moment().toDate(),
            type: type,
            moderator: moderator,
            reason: reason
        };

        if (duration) {
            var date = moment();

            sanction.expiry = date.add(duration, 'ms').toDate();
            sanction.duration = duration;
        }

        const result = await Sanctions.findOneAndUpdate(
            { user: member.id },
            { $set: { muted: muted }, $push: { sanctions: sanction } }
        ).exec();

        if (!result) {
            await new Sanctions({
                user: member.id,
                muted: muted,
                sanctions: sanction
            }).save();
        }
    }

    async demuteUser(client, member, moderator, command = false, reason = "Automatique") {
        const modNotif = this.getSanctionEmbed('#43b581', 'UNMUTE', moderator, member.user, reason);
        const userNotif = "Vous venez d'être demute du serveur de McFly & Carlito.";

        await Sanctions.findOneAndUpdate(
            { user: member.id },
            { muted: false }
        ).exec();

        if (command) {
            clearTimeout(client.mutedUsers.get(member.id));
            client.mutedUsers.delete(member.id);
        }

        await member.roles.remove(roles.muted);
        await userUtils.sendNotificationToUser(member.user, userNotif, modNotif);
        await client.sanctions.send(modNotif);
    }

    async retrieveMutedMember(client, guild, member = null) {
        const match = member ? { user: member.id, muted: true } : { muted: true };
        const queryResult = await Sanctions.aggregate([
            { $match: match },
            { $unwind: '$sanctions' },
            { $match: { 'sanctions.type': 'MUTE' } },
            {
                $group: {
                    _id: '$sanctions.type',
                    user: { $first: '$user' },
                    lastMute: { $max: '$sanctions.expiry' }
                }
            }
        ]).exec();

        queryResult.forEach(async sanction => {

            try {
                if (!member) {
                    member = await guild.members.fetch(sanction.user);
                }
            } catch(error) { }

            if (member) {
                const muteDate = moment(sanction.lastMute);
                const now = moment();

                if (now > muteDate) {
                    await this.demuteUser(client, member, client.user);
                } else {
                    const duration = muteDate.diff(now);

                    await member.roles.add(roles.muted);
                    client.mutedUsers.set(member.id, setTimeout(async () => {
                        await client.moderation.demuteUser(client, member, client.user);
                    }, duration));
                }
            }
        });
    }

    getSanctionEmbed(color, type, moderator, user, reason, duration = "") {
        return new Discord.MessageEmbed()
            .setColor(color)
            .setAuthor(`[${type}] ${user.tag}` + duration, user.displayAvatarURL())
            .addFields(
                { name: "User", value: user, inline: true },
                { name: "Modérateur", value: moderator, inline: true },
                { name: "Raison", value: reason, inline: true }
            );
    }
};
