const Discord = require('discord.js');
const e = require('express');
const Match = require('../../models/match');
const Wallet = require('../../models/wallet');

module.exports = class Animation {

    constructor(client) {
        this.client = client;
    }

    async createMatch(team1, rate1, team2, rate2, nullRate, date) {
        var match = await Match.findOne({currentMatchId: {$exists: true}});
        var matchId;

        if (!match) {
            await new Match({currentMatchId: 1}).save();
            matchId = 1;
        } else {
            matchId = match.currentMatchId + 1;
            await Match.updateOne({currentMatchId: {$exists: true}}, {$inc: {currentMatchId: 1}});
        }

        await new Match({
            matchId: matchId,
            date: date,
            open: true,
            finish: false,
            team1: team1,
            rate1: rate1,
            team2: team2,
            rate2: rate2,
            nullRate: nullRate
        }).save();

        return matchId;
    }

    async getMatch(id) {
        return await Match.findOne({matchId: id});
    }

    async closeMatch(id) {
        return await Match.updateOne({matchId: id}, {open: false});
    }

    async openMatch(id) {
        return await Match.updateOne({matchId: id}, {open: true});
    }

    async finishMatch(id) {
        return await Match.updateOne({matchId: id}, {finish: true});
    }

    async giveEarning(userId, bet, rate) {
        return await Wallet.updateOne({userId: userId}, {$inc: {amount: bet * rate}});
    }

    async haveProno(id, userId) {
        const match = await Match.findOne({matchId: id, "pronos.userId": userId});

        return match ? true : false;
    }

    async getProno(id, userId) {
        return await Match.findOne({matchId: id}).select({ pronos: {$elemMatch: {userId: userId}} });
    }

    async pushProno(id, userId, team, money) {
        const match = await this.getMatch(id);

        match.pronos.push({
            userId: userId,
            team: team,
            money: money
        });
        await match.save();
    }

    async deleteOldProno(id, userId) {
        const prono = await Match.findOne({matchId: id}).select({ pronos: {$elemMatch: {userId: userId}} });

        if (prono) {
            await this.sumWallet(userId, prono.pronos[0].money);
            await Match.updateOne({matchId: id}, {$pull: {pronos: { userId: userId }}});
        }

        return prono.pronos[0].money;
    }

    async getWallet(userId) {
        const wallet = await Wallet.findOne({userId: userId});

        if (!wallet) {
            await new Wallet({
                userId: userId,
                amount: 100
            }).save();

            return 100;
        } else {
            return wallet.amount;
        }
    }

    async substractWallet(userId, amount) {
        return await Wallet.updateOne({userId: userId}, {$inc: {amount: -amount}});
    }

    async sumWallet(userId, amount) {
        return await Wallet.updateOne({userId: userId}, {$inc: {amount: amount}});
    }

    async getTop10() {
        return await Wallet.find({userId: {$ne: "178512804347772928"}}).sort({amount:-1}).limit(10);
    }
};