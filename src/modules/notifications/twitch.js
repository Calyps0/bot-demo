const { notifications, channels } = require.main.require('./config.json');

const { ApiClient  } = require('twitch');
const { ClientCredentialsAuthProvider } = require('twitch-auth');
const { ReverseProxyAdapter, WebHookListener } = require('twitch-webhooks');

const authProvider = new ClientCredentialsAuthProvider(notifications.twitch.clientId, notifications.twitch.clientSecret);
const apiClient = new ApiClient({ authProvider });

var discordClient;

const handleFeed = (stream) => {
    discordClient.channels.fetch(channels.live_notif)
        .then(channel => {
            channel.send(`@everyone\nBonsoèèèèèèr !! **McFly & Carlito** sont en live sur Twitch ! Go checker ça <:dab:696090110265131063> \nhttps://www.twitch.tv/${stream.userDisplayName}`);
        }).catch(error => client.logger.error(error));
}

const load = async (client, app) => {

    const listener = new WebHookListener(apiClient, new ReverseProxyAdapter({
        "hostName": notifications.host,
        "listenerPort": notifications.port,
        "port": notifications.port,
        "pathPrefix": "/webhooks/twitch",
        "ssl": false
    }));

    listener.applyMiddleware(app);

    let prevStream = await apiClient.helix.streams.getStreamByUserId(notifications.twitch.channel);

    const subscription = await listener.subscribeToStreamChanges(notifications.twitch.channel, async (stream) => {
        if (stream && !prevStream) {
            client.logger.info("Nouveau live lancé sur la chaîne Twitch");
            handleFeed(stream);
        }
        prevStream = stream;
    });

    await subscription.start();

    client.logger.info("Souscription à la chaine Twitch réussie");

    discordClient = client;
}

module.exports.load = load;
