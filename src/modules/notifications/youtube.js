const { notifications, channels } = require.main.require('./config.json');

const pubSubHubbub = require('pubsubhubbub');
const axios = require('axios');
const xml = require('xml2js');
const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const hub = "http://pubsubhubbub.appspot.com/";
var date = new Date().getTime();
var videos = [];
var lives = [];

const handleFeed = (client, data) => {
    xml.parseString(data.feed.toString('utf-8'), (err, res) => {
        if (err) return client.logger.error(err);
        if (!res.feed.hasOwnProperty('entry')) return;

        const channelId = res.feed.entry[0]['yt:channelId'][0],
            videoId = res.feed.entry[0]['yt:videoId'][0],
            author = res.feed.entry[0]['author'][0].name[0],
            link = res.feed.entry[0]['link'][0].$.href,
            published = Date.parse(res.feed.entry[0]['published'][0]);

        if (published < date) return;

        axios.get(link).then(res => {
            const dom = new JSDOM(res.data);
            const liveMeta = dom.window.document.querySelector("span[itemprop='publication']");

            if (liveMeta && liveMeta.childElementCount < 3) {
                if (lives.includes(videoId)) return;

                client.channels.fetch(channels.live_notif)
                    .then(channel => {
                        channel.send(`@everyone\nBonsoèèèèèèr !! **${author}** sont en live sur Youtube ! Go checker ça <:dab:696090110265131063> \n${link}`);
                        lives.push(videoId);
                        client.logger.info(`Nouveau live Youtube reçu`);
                    }).catch(error => client.logger.error(error));
            } else {
                if (videos.includes(videoId)) return;

                client.channels.fetch(channels.video_notif)
                    .then(channel => {
                        if (channelId == notifications.youtube.main) {
                            channel.send(`@everyone\nBonsoèèèèèèr !! **${author}** ont posté une nouvelle vidéo sur leur chaîne Youtube ! Go checker ça <:dab:696090110265131063> \n${link}`);
                            client.logger.info(`Nouvelle vidéo sur la chaîne Youtube principale reçue`);
                        } else {
                            channel.send(`@everyone\nBonsoèèèèèèr !! **McFly & Carlito** ont posté une nouvelle vidéo sur leur chaîne Youtube secondaire ! Go checker ça <:dab:696090110265131063> \n${link}`);
                            client.logger.info(`Nouvelle vidéo sur la chaîne Youtube secondaire reçue`);
                        }
                        videos.push(videoId);
                    }).catch(error => client.logger.error(error));
            }
        });
        date = published;
    });
};

const load = async (client, app) => {

    const pubSubSubscriber = pubSubHubbub.createServer({
        "callbackUrl": `http://${notifications.host}:${notifications.port}/webhooks/youtube`,
        "leaseSeconds": 430000
    });

    app.use("/webhooks/youtube", pubSubSubscriber.listener());

    setInterval(function subscribe() {
        pubSubSubscriber.subscribe(`https://www.youtube.com/xml/feeds/videos.xml?channel_id=${notifications.youtube.main}`, hub);
        pubSubSubscriber.subscribe(`https://www.youtube.com/xml/feeds/videos.xml?channel_id=${notifications.youtube.second}`, hub);
        return subscribe;
    }(), 432000000); // 5 Days

    pubSubSubscriber.on("subscribe", function(data) {
        channelId = data.topic.match(/channel_id=(.*)/)[1];
        client.logger.info(`Souscription à la chaine Youtube ${channelId} réussie !`);
    });

    pubSubSubscriber.on("feed", (data) => {
        handleFeed(client, data);
    });
}

module.exports.load = load;
