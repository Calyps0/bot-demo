const { channels, roles } =  require.main.require('./config.json');

exports.execute = async (client, oldState, newState) => {

    /*
     * Ajoute / Supprime le rôle participant dans le channel SAS Animation
    */
    if (newState.channelID === channels.sas_animation) {
        newState.member.roles.add(roles.participant);
        
        if (client.anim.isStarted) {
            client.anim.updatePaticipants(newState.channel);
        }
    } else if (oldState.channelID === channels.sas_animation) {
        oldState.member.roles.remove(roles.participant).catch(error => {
            client.logger.error(new Error("Impossible de retirer le grade participant"));
        });
    }

    /*
     * Ajoute / Supprime le rôle viewer dans le channel Live
     */
    if (newState.channelID === channels.live) {
        newState.member.roles.add(roles.viewer);
    } else if (oldState.channelID === channels.live) {
        oldState.member.roles.remove(roles.viewer);
    }

};