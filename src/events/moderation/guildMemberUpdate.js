const Discord = require('discord.js');

exports.execute = async (client, oldMember, newMember) => {

    if (oldMember.nickname === newMember.nickname) {
        return;
    }

    const oldNickname = oldMember.nickname ? oldMember.nickname : oldMember.user.username;
    const newNickname = newMember.nickname ? newMember.nickname : newMember.user.username;

    const fetchedLogs = await oldMember.guild.fetchAuditLogs({
		limit: 1,
		type: 'MEMBER_UPDATE'
    });
    const updateLog = fetchedLogs.entries.first();

    const log = new Discord.MessageEmbed()
        .setColor('#faa51b')
        .setAuthor(oldMember.user.tag, oldMember.user.displayAvatarURL())
        .setThumbnail(oldMember.user.displayAvatarURL())
        .addFields(
            { name: "Avant", value: `\`${oldNickname}\``, inline: true },
            { name: "Après", value: `\`${newNickname}\``, inline: true }
        )
        .setFooter(`User ID: ${oldMember.id}`)
        .setTimestamp();

    if (updateLog && updateLog.executor.id !== updateLog.target.id && updateLog.target.id === oldMember.id) {
        log.setDescription(`:pencil: ${updateLog.executor} a changé le pseudo de ${oldMember.user}`);
    } else {
        log.setDescription(`:pencil: ${oldMember.user} a changé son pseudo`);
    }

    await client.modlogs.send(log);
};
