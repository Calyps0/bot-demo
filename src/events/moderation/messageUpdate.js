const Discord = require('discord.js');

exports.execute = async (client, oldMessage, newMessage) => {

    if (!oldMessage.content || !newMessage.content ||
            oldMessage.author.id === oldMessage.client.user.id ||
            oldMessage.content === newMessage.content) {
        return;
    }

    const user = oldMessage.author;

    await client.modlogs.send(new Discord.MessageEmbed()
        .setColor('#337fd5')
        .setAuthor(user.tag, user.displayAvatarURL())
        .setThumbnail(user.displayAvatarURL())
        .setDescription(`:pencil2: Message édité dans <#${oldMessage.channel.id}> [Voir le message](${oldMessage.url})`)
        .addFields(
            { name: "Avant", value: oldMessage.content },
            { name: "Après", value: newMessage.content }
        )
        .setFooter(`User ID: ${user.id}`)
        .setTimestamp()
    );

};