const Discord = require('discord.js');

const moment = require('moment-timezone');

exports.execute = async (client, member) => {

    const creation = moment(member.user.createdAt).format('dddd D MMMM YYYY à HH:mm');

    await client.moderation.retrieveMutedMember(client, null, member);

    await client.modlogs.send(new Discord.MessageEmbed()
        .setColor('#43b581')
        .setAuthor(member.user.tag, member.user.displayAvatarURL())
        .setDescription(`:inbox_tray: ${member.user} a rejoint le serveur`)
        .setThumbnail(member.user.displayAvatarURL())
        .addFields(
            { name: "Date de création du compte", value: creation[0].toUpperCase() + creation.slice(1) }
        )
        .setFooter(`User ID: ${member.id}`)
        .setTimestamp()
    );

};