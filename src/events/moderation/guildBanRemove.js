const Discord = require('discord.js');

exports.execute = async (client, guild, user) => {

    await client.modlogs.send(new Discord.MessageEmbed()
        .setColor('#43b581')
        .setAuthor(user.tag, user.displayAvatarURL())
        .setDescription(`:unlock: ${user} a été débanni`)
        .setThumbnail(user.displayAvatarURL())
        .setFooter(`User ID: ${user.id}`)
        .setTimestamp()
    );

};