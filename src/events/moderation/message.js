const Discord = require('discord.js');
const AntiSpam = require('discord-anti-spam');

const antiSpam = new AntiSpam({
	warnThreshold: 3, // Amount of messages sent in a row that will cause a warning.
    maxDuplicatesMute: 2000,
	kickThreshold: 7, // Amount of messages sent in a row that will cause a kick.
	maxInterval: 2000, // Amount of time (in milliseconds) in which messages are considered spam.
	warnMessage: '{@user}, merci d\'arrêter de spam.', // Message that will be sent in chat upon warning a user.
	kickMessage: '**{user_tag}** a été kick pour spam.', // Message that will be sent in chat upon kicking a user.
	maxDuplicatesWarn: 1, // Amount of duplicate messages that trigger a warning.
	maxDuplicatesKick: 10, // Amount of duplicate messages that trigger a warning.
	ignoreBots: true, // Ignore bot messages.
	removeMessages: true, // If the bot should remove all the spam messages when taking action on a user!
    removeBotMessages: true,
    muteEnabled: false,
    banEnnabled: false
});

exports.execute = async (client, message) => {
    //antiSpam.message(message);
};
