const Discord = require('discord.js');

exports.execute = async (client, guild, user) => {

    const fetchedLogs = await guild.fetchAuditLogs({
        limit: 1,
        type: 'MEMBER_BAN_ADD',
    });
    const banLog = fetchedLogs.entries.first();
    const reason = banLog.reason ? banLog.reason : "Aucune raison fournie";

    if (banLog && banLog.executor.id !== client.user.id) {
        const modNotif = client.moderation.getSanctionEmbed('#ff470f', 'BAN', banLog.executor, banLog.target, reason);

        await client.sanctions.send(modNotif);
    }

    await client.modlogs.send(new Discord.MessageEmbed()
        .setColor('#ff470f')
        .setAuthor(user.tag, user.displayAvatarURL())
        .setDescription(`:lock: ${user} a été banni pour la raison suivante : **${reason}**`)
        .setThumbnail(user.displayAvatarURL())
        .setFooter(`User ID: ${user.id}`)
        .setTimestamp()
    );

};