const Discord = require('discord.js');

async function sendModlog(message, deletionLog, attachment = null) {
    const author = message.author;
    var content = message.content;
    var log = new Discord.MessageEmbed()
        .setColor('#ff470f')
        .setAuthor(author.tag, author.displayAvatarURL())
        .setThumbnail(author.displayAvatarURL())
        .setFooter(`User ID: ${author.id}`)
        .setTimestamp();

    // Si le message possède un fichier attaché
    if (attachment) {
        const extImg = ['.jpg', '.jpeg', '.png', '.gif', '.bmp'];
        const ext = attachment.name.match(/\.[0-9a-z]+$/i);

        if (extImg.includes(ext[0])) {
            log.setImage(attachment.proxyURL);
        } else {
            content += '\n';
            content += ':file_folder: __*Ce message avait une pièce jointe*__ :file_folder: ' + attachment.proxyURL;
        }
    }

    if (!deletionLog || deletionLog.target.id !== message.author.id) {
        log.setDescription(`**:wastebasket: Message de ${author} supprimé dans <#${message.channel.id}>**\n${content}`);
    } else {
        log.setDescription(`**:wastebasket: Message de ${author} supprimé dans <#${message.channel.id}> par ${deletionLog.executor}**\n${content}`);
    }

    await author.client.modlogs.send(log);
}

exports.execute = async (client, message) => {
    if (message.system || (!message.content && !message.attachments)) {
        return;
    }

    const fetchedLogs = await message.guild.fetchAuditLogs({
		limit: 1,
		type: 'MESSAGE_DELETE',
    });
    const deletionLog = fetchedLogs.entries.first();

    if (message.attachments.size) {
        message.attachments.forEach(async attachment => {
            await sendModlog(message, deletionLog, attachment);
        });
    } else {
        await sendModlog(message, deletionLog);
    }
};