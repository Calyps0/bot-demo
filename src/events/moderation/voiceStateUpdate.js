const Discord = require('discord.js');

exports.execute = async (client, oldState, newState) => {

    const user = oldState.member.user;
    const oldChannel = oldState.channel;
    const newChannel = newState.channel;

    const log = new Discord.MessageEmbed()
        .setAuthor(user.tag, user.displayAvatarURL())
        .setThumbnail(user.displayAvatarURL())
        .setFooter(`User ID: ${user.id}`)
        .setTimestamp();

    if (!oldChannel && newChannel) {
        log.setDescription(`:loud_sound: ${user} a rejoint le channel \`${newChannel.name}\``).setColor('#43b581');
    } else if (oldChannel && newChannel) {
        if (oldChannel.id === newChannel.id) {
            return;
        }

        log.setDescription(`:loud_sound: ${user} est passé du channel \`${oldChannel.name}\` au channel \`${newChannel.name}\``).setColor('#43b581');
    } else if (oldChannel && !newChannel) {
        log.setDescription(`:sound: ${user} a quitté le channel \`${oldChannel.name}\``).setColor('#ff470f');
    }

    await client.modlogs.send(log);

};