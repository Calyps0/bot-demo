const Discord = require('discord.js');

exports.execute = async (client, member) => {

    if (client.mutedUsers.has(member.id)) {
        clearTimeout(client.mutedUsers.get(member.id));
        client.mutedUsers.delete(member.id);
    }

    await client.modlogs.send(new Discord.MessageEmbed()
        .setColor('#ff470f')
        .setAuthor(member.user.tag, member.user.displayAvatarURL())
        .setDescription(`:outbox_tray: ${member.user} a quitté le serveur`)
        .setThumbnail(member.user.displayAvatarURL())
        .setFooter(`User ID: ${member.id}`)
        .setTimestamp()
    );

};