// Log quand le bot est prêt à être utilisé
exports.execute = async (client) => {

    client.logger.info("Le bot est en ligne !");

    client.user.setPresence(
        {
            "activity": {
                "name": 'McFly & Carlito',
                "type": "WATCHING",
            }
        }
    ).then().catch(error => client.logger.error(error).error);
};