const Discord = require('discord.js');
const { prefixCommand } = require.main.require('./config.json');

const formatUsage = (name, usages) => {
    var result = "";

    usages.forEach(usage => {
        result += `${usage.name}:\n \`${prefixCommand}${name} ${usage.usage}\`\n`;
    });
    return result;
};

const hasRole = (message, roles) => {
    if (message.member.hasPermission("ADMINISTRATOR")) {
        return true;
    }
    for (let i = 0; i < roles.length; i++) {
        if (message.member.roles.cache.has(roles[i])) {
            return true;
        }
    }
    return false;
};

// Gestion des commandes du bot
exports.execute = async (client, message) => {
    if (message.author.bot || !message.content.startsWith(prefixCommand)) return;

    const args = message.content.slice(prefixCommand.length).trim().split(/ +/);
    const commandName = args.shift().toLowerCase();
    const command = client.commands.get(commandName);

    if (!command) return;

    if (command.roles && !hasRole(message, command.roles)) {
        return;
    }
    
    if (command.args && !args.length) {
        return message.channel.send(new Discord.MessageEmbed()
            .setColor("F22626")
            .setTitle(`Commande ${prefixCommand}${command.name}\n\u200B`)
            .addFields(
                { name: "Description", value: `${command.description}\n\u200B` },
                { name: "Usage", value: formatUsage(commandName, command.usages) + "\u200B"  },
                { name: "Exemple", value: formatUsage(commandName, command.examples) },
            )
        ).then().catch(error => client.logger.error(error));
    }

	try {
        await command.execute(client, message, args);
	} catch (error) {
        console.log(error);
        message.reply("une erreur est suvenue avec l\'utlisation de cette commande");
        client.logger.error(error);
	}
};
