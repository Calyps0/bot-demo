const Discord = require('discord.js');
const dedent = require('dedent-js');

const { groups } = require.main.require('./config.json');

exports.execute = async (client, oldState, newState) => {

    if (newState.selfVideo) {

        for (let i = 0; i < groups.team.length; i++) {
            if (newState.member.roles.cache.has(groups.team[i])) {
                return;
            }
        }

        const user = oldState.member.user;
        var description = `:camera: ${user} a été déconnecté du salon \`${newState.channel.name}\` car sa caméra a été activée`;

        newState.kick();

        try {
            await user.send(`Bonjour ${user.username}\n\n` +

                `Vous avez été déconnecté du salon \`${newState.channel.name}\` car vous avez activé votre caméra.\n` +
                `L’utilisation de la caméra est strictement interdite.\n\n` +

                `Comprenez que cela permet de protéger votre vie privée et d'éviter les utilisations malveillantes de cette fonctionnalité.\n\n` +
        
                `L'équipe de modération`
            );
        } catch(e) {
            description += '\n\n:no_entry: Utilisateur non notifié car ses MP sont désactivés';
        }

        await client.modlogs.send(new Discord.MessageEmbed()
            .setAuthor(user.tag, user.displayAvatarURL())
            .setDescription(description)
            .setColor('#ff470f')
            .setThumbnail(user.displayAvatarURL())
            .setFooter(`User ID: ${user.id}`)
            .setTimestamp()
        );
    }
};