const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;
const chalk = require('chalk');

const info = chalk.bold.blue;
const error = chalk.bold.red;

const myFormat = printf(({ level, message, label, timestamp }) => {

    level = level.toUpperCase();

    if (label === "console") {
        switch(level) {
            case "INFO":
                level = info(level);
                break;
            case "ERROR":
                level = error(level);
                message = "Une erreur est suvenue. Consultez le fichier errors.log pour plus d'informations.";
                break;
            default:
                return;
        }
    }

    return `${timestamp} ${level} ${message}`;
});


const loadLogger = async (client) => {

    client.logger = createLogger({
        level: 'info',
        format: combine(
            label({ label: "file" }),
            timestamp(),
            myFormat,
        ),
        transports: [
            new transports.File({ filename: "logs/errors.log", level: "error" }),
            new transports.File({ filename: "logs/infos.log", level: "info" }),
            new transports.Console({
                format: combine(
                    label({ label: "console" }),
                    myFormat
                )
            })
        ],
    });
    
    client.logger.error = err => {
        if (err instanceof Error) {
            client.logger.log({ level: 'error', message: `${err.stack || err}` });
        } else {
            client.logger.log({ level: 'error', message: err });
        }
    };
};

module.exports.loadLogger = loadLogger;