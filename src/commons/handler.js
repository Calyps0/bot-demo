const Discord = require('discord.js');
const path = require("path");
const files = require("./files");

// Chargement des événements
const eventsHandler = async (client) => {

    client.logger.info("Chargement des événements ...");

    const eventsFiles = files.getAllFiles("events");

    eventsFiles.forEach(eventFile => {
        const event = require(`${eventFile}`);
        const eventName = path.basename(eventFile).split(".").shift();

        client.on(eventName, (...args) => {
            event.execute(client, ...args).catch((error) => console.log(error));
        });

        delete require.cache[require.resolve(`${eventFile}`)];
    });

    client.logger.info(`${eventsFiles.length} événements chargés`);
};

// Chargement des commandes
const commandsHandler = async(client) => {

    client.logger.info("Chargement des commandes ...");

    const commandsFiles = files.getAllFiles("commands");
    
    client.commands = new Discord.Collection();
    for (const commandFile of commandsFiles) {
        const command = require(`${commandFile}`);

        client.commands.set(command.name, command);
        delete require.cache[require.resolve(`${commandFile}`)];
    }

    client.logger.info(`${commandsFiles.length} commandes chargées`);
};

module.exports.eventsHandler = eventsHandler;
module.exports.commandsHandler = commandsHandler;