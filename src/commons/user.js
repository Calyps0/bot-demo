const dedent = require('dedent-js');

async function getUserFromMention(message, mention) {
    const matches = mention.match(/^<@!?(\d+)>$/);

    if (!matches) return;

    try {
        return await message.guild.members.fetch(matches[1]);
    } catch(error) {
        if (error.code !== 10007) {
            console.log(error);
        }
        return;
    }
}

async function getMemberFromId(client, message, id) {
    try {
        return await message.guild.members.fetch(id);
    } catch(error) {
        const user = await client.users.fetch(id);

        return {
            displayName: user.username
        };
    }
}

async function sendNotificationToUser(user, reason, modNotif) {
    try {
        await user.send(dedent`Bonjour ${user.username},
        
        ${reason}

        L'équipe de modération
        `);
    } catch(e) {
        modNotif.setDescription(":no_entry: Utilisateur non notifié car ses MP sont désactivés");
    }
}

function checkRoleInList(member, roles) {
    return member.roles.cache.some(role => roles.includes(role.id));
}

module.exports.getUserFromMention = getUserFromMention;
module.exports.getMemberFromId = getMemberFromId;
module.exports.sendNotificationToUser = sendNotificationToUser;
module.exports.checkRoleInList = checkRoleInList;