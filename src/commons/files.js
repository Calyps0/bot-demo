const fs = require('fs');
const { join } = require('path');

// Récupréation de tous les fichiers javascript d'un dossier
const getAllFiles = (dirPath, arrayOfFiles) => {
  const dir = join(__dirname, "..", dirPath);
  
  files = fs.readdirSync(dir);
  arrayOfFiles = arrayOfFiles || [];

  files.forEach(function(file) {
    var filePath = join(dir, file);

    if (fs.statSync(filePath).isDirectory()) {
      arrayOfFiles = getAllFiles(join(dirPath, file), arrayOfFiles);
    } else {
      arrayOfFiles.push(join(dir, file));
    }
  });
  return arrayOfFiles;
}

module.exports.getAllFiles = getAllFiles;