// Discord
const Discord = require('discord.js');
const client = new Discord.Client();

// Express
var express = require('express');
var app = express();

// Mongoose
const mongoose = require('mongoose');

// Moment
const moment = require('moment-timezone');

// Bot
const config = require('./config.json');
const handler = require('./commons/handler');
const logger = require('./commons/logger');
const twitch = require('./modules/notifications/twitch');
const youtube = require('./modules/notifications/youtube');

const Animation = require('./modules/animation/anim');
const Scoreboard = require('./modules/animation/scoreboard');
const Moderation = require('./modules/moderation/moderation');

const load = async () => {

    await logger.loadLogger(client);
    await client.login(config.token);

    /*
     * Initialisation channels
     */
    client.moderation = new Moderation();
    client.modlogs = await client.channels.fetch(config.channels.modlogs);
    client.sanctions = await client.channels.fetch(config.channels.sanctions);

    await mongoose.connect('mongodb://database/bot_mc', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
    });

    app.listen(config.notifications.port, () => {
        client.logger.info(`Server WEB lancé sur le port ${config.notifications.port}`);
    });

    /*
     * Initialisation des modules
     */
    await handler.eventsHandler(client);
    await handler.commandsHandler(client);
    await youtube.load(client, app);
    await twitch.load(client, app);

    moment.tz.setDefault('Europe/Paris');
    moment.locale('fr');
};

load();