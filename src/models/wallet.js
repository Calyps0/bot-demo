const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Wallet = new Schema({
    userId: String,
    amount: Number
});

module.exports = mongoose.model('Wallet', Wallet);