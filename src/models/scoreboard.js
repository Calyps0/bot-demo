const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Scoreboard = new Schema({
    user: {
        type: String,
        match: /[0-9]{18}/,
        required: true
    },
    score: {
        type: Number,
        min: 0,
        required: true
    }
});

module.exports = mongoose.model('Scoreboard', Scoreboard);