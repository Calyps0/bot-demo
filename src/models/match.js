const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Match = new Schema({
    currentMatchId: Number,
    matchId: Number,
    date: String,
    open: Boolean,
    finish: Boolean,
    team1: String,
    rate1: Number,
    team2: String,
    rate2: Number,
    nullRate: Number,
    pronos: [
        {
            userId: String,
            team: String,
            money: Number
        }
    ]
});

module.exports = mongoose.model('Match', Match);