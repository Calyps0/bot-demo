const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Sanction = new Schema({
    date: {
        type: Date,
        required: true
    },
    type: {
        type: String,
        enum: ['WARN', 'MUTE', 'KICK'],
        required: true
    },
    duration: { type: Number },
    expiry: { type: Date },
    moderator: {
        type: String,
        match: /^((.+?)#\d{4})/,
        required: true
    },
    reason: {
        type: String,
        required: true
    }
});

const Sanctions = new Schema({
    user: {
        type: String,
        match: /[0-9]{18}/,
        required: true
    },
    muted: {
        type: Boolean,
        required: true
    },
    sanctions: [Sanction]
});

module.exports = mongoose.model('Sanctions', Sanctions);
